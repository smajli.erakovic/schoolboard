# Schoolboard

Design a system that is responsible for the managing the grades for a list of students.

Calculate the average of the grades for a given student, identify if he has passed or failed and return the student’s statistic.