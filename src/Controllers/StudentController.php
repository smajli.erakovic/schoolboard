<?php

namespace Src\Controllers;

use Src\TableGateways\StudentGateway;

class StudentController
{

    private $db;
    private $requestMethod;
    private $studentId;

    private $studentGateway;

    public function __construct($db, $requestMethod, $studentId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->studentId = $studentId;

        $this->studentGateway = new StudentGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->studentId) {
                    $request = $this->getStudent($this->studentId);
                } else {
                    $request = $this->getAllStudents();
                }
                break;
            default:
                $request = $this->requestNotFound();
                break;
        }

        header($request['status_code_header']);

        if ($request['body']) {
            echo $request['body'];
        }
    }

    private function getStudent($id)
    {
        $result = $this->studentGateway->find($id);

        if (!$result) {
            return $this->requestNotFound();
        }

        $request['status_code_header'] = 'HTTP/1.1 200 OK';
        $request['body'] = json_encode($result);

        return $request;
    }

    private function getAllStudents()
    {
        $result = $this->studentGateway->findAll();

        $request['status_code_header'] = 'HTTP/1.1 200 OK';
        $request['body'] = json_encode($result);

        return $request;
    }

    private function requestNotFound()
    {
        $request['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $request['body'] = null;

        return $request;
    }
}