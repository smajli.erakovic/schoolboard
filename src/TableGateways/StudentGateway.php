<?php

namespace Src\TableGateways;

use Src\Requests\JsonFormat;
use Src\Requests\XmlFormat;

class StudentGateway
{
    const CSM = 'CSM';
    const CSMB = 'CSMB';
    const CSM_MIN_GRADE = 6;
    const CSMB_MIN_GRADE = 8;

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT 
                `id`, `name`, `schoolboard`, `grades`
            FROM
                `students`;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            return (new JsonFormat($result))->get();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT 
                `id`, `name`, `schoolboard`, `grades`
            FROM
                `students`
            WHERE `id` = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (!$result) {
                return $result;
            }

            $result = $this->parseResult($result);

            if ($result[0]['schoolboard'] === $this::CSM) {

                return (new JsonFormat($result))->get();
            }

            return (new XmlFormat($result[0]))->get();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /**
     * Prepare Response Results
     *
     * @param $data
     * @return array
     */
    private function parseResult($data)
    {
        $result = [];

        foreach ($data as $student) {
            array_push($result, [
                'id' => $student['id'],
                'name' => $student['name'],
                'schoolboard' => $student['schoolboard'],
                'grades' => $this->getGrades($student),
                'average' => $this->getStudentAverageGrade($student),
                'final_result' => $this->passed($student) ? 'passed' : 'failed'
            ]);
        }

        return $result;
    }

    private function getGrades($data): array
    {
        if (key_exists('grades', $data)) {
            return json_decode($data['grades']);
        }

        return [];
    }

    /**
     * Get average grade
     *
     * @param array $grades
     * @return float
     */
    private function getAverageGrade(array $grades): float
    {
        if (count($grades) === 0) return 0;

        return array_sum($grades) / count($grades);
    }

    /**
     * Calculate Student average grade
     *
     * @param $data
     * @return float
     */
    private function getStudentAverageGrade($data): float
    {
        if (!key_exists('schoolboard', $data)) {
            return 0;
        }

        $grades = $this->getGrades($data);

        if ($data['schoolboard'] === $this::CSM) {
            return $this->getAverageGrade($grades);
        }

        $grades = $this->getGradesWithoutLowestGrade($grades);

        return $this->getAverageGrade($grades);
    }

    /**
     * Removes the lowest grade from array if it has more than 2 grades and use that array for average grade calculation
     *
     * @param array $grades
     * @return array
     */
    private function getGradesWithoutLowestGrade(array $grades): array
    {
        if (count($grades) > 2) {
            // remove lowest grade from array
            $lowestGrade = min($grades);
            $lowestGradeKey = array_search($lowestGrade, $grades);

            $grades = array_splice($grades, $lowestGradeKey, 1);
        }

        return $grades;
    }

    /**
     * Is Student passed
     *
     * @param $data
     * @return bool
     */
    private function passed($data): bool
    {
        if (!key_exists('schoolboard', $data)) {
            return 0;
        }

        if ($data['schoolboard'] === $this::CSM) {
            $averageGrade = $this->getStudentAverageGrade($data);

            return $averageGrade > $this::CSM_MIN_GRADE;
        }

        $grades = $this->getGradesWithoutLowestGrade($this->getGrades($data));

        return max($grades) > $this::CSMB_MIN_GRADE;
    }
}