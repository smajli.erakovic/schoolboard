<?php
namespace Src\Requests;

abstract class ResponseFormat {
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function get()
    {
        return $this->data;
    }
}