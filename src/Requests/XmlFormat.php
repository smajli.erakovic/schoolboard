<?php

namespace Src\Requests;

use Src\Requests\ResponseFormat;

class XmlFormat extends ResponseFormat
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function get()
    {
        $xml = new \SimpleXMLElement('<root/>');
        $this->xmlPrepare($xml, $this->data);

        return $xml->asXML();
    }

    private function xmlPrepare(\SimpleXMLElement $xml, array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $new_object = $xml->addChild($key);
                $this->xmlPrepare($new_object, $value);
            } else {
                // if the key is an integer, it needs text with it to actually work.
                if ($key === (int) $key) {
                    $key = "key_$key";
                }

                $xml->addChild($key, $value);
            }
        }
    }
}