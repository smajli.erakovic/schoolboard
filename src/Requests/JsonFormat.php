<?php
namespace Src\Requests;

use Src\Requests\ResponseFormat;

class JsonFormat extends ResponseFormat {
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function get()
    {
        return json_encode($this->data);
    }
}